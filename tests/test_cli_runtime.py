import json
import logging
import os.path
import shutil
import sys
import tempfile
import unittest
import yaml

import pandas

COMMON_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'common-primitives')
# NOTE: This insertion should appear before any code attempting to resolve or load primitives,
# so the git submodule version of `common-primitives` is looked at first.
sys.path.insert(0, COMMON_PRIMITIVES_DIR)

TEST_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'primitives')
sys.path.insert(0, TEST_PRIMITIVES_DIR)

from common_primitives.column_parser import ColumnParserPrimitive
from common_primitives.construct_predictions import ConstructPredictionsPrimitive
from common_primitives.dataset_to_dataframe import DatasetToDataFramePrimitive
from common_primitives.random_forest import RandomForestClassifierPrimitive
from common_primitives.no_split import NoSplitDatasetSplitPrimitive

from test_primitives.random_classifier import RandomClassifierPrimitive
from test_primitives.fake_score import FakeScorePrimitive

from d3m import cli, utils, index
from d3m.contrib.primitives.compute_scores import ComputeScoresPrimitive
from d3m.metadata import pipeline_run

PROBLEM_DIR = os.path.join(os.path.dirname(__file__), 'data', 'problems')
DATASET_DIR = os.path.join(os.path.dirname(__file__), 'data', 'datasets')
PIPELINE_DIR = os.path.join(os.path.dirname(__file__), 'data', 'pipelines')


class TestCLIRuntime(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    @classmethod
    def setUpClass(cls):
        to_register = {
            'd3m.primitives.data_transformation.dataset_to_dataframe.Common': DatasetToDataFramePrimitive,
            'd3m.primitives.classification.random_forest.Common': RandomForestClassifierPrimitive,
            'd3m.primitives.classification.random_classifier.Test': RandomClassifierPrimitive,
            'd3m.primitives.data_transformation.column_parser.Common': ColumnParserPrimitive,
            'd3m.primitives.data_transformation.construct_predictions.Common': ConstructPredictionsPrimitive,
            'd3m.primitives.evaluation.no_split_dataset_split.Common': NoSplitDatasetSplitPrimitive,
            'd3m.primitives.evaluation.compute_scores.Test': FakeScorePrimitive,
            # We do not have to load this primitive, but loading it here prevents the package from loading all primitives.
            'd3m.primitives.evaluation.compute_scores.Core': ComputeScoresPrimitive,
        }

        # To hide any logging or stdout output.
        with utils.silence():
            for python_path, primitive in to_register.items():
                index.register_primitive(python_path, primitive)

    def _call_cli_runtime_without_fail(self, arg):
        logger = logging.getLogger('d3m.runtime')
        arg.insert(1, 'runtime')
        try:
            with utils.silence():
                with self.assertLogs(logger=logger) as cm:
                    # So that at least one message is logged.
                    logger.warning("Debugging.")
                    cli.main(arg)
        except Exception as e:
            self.fail(e)

        # We skip our "debugging" message.
        return cm.records[1:]

    def _assert_valid_saved_pipeline_runs(self, pipeline_run_save_path):
        with open(pipeline_run_save_path, 'r') as f:
            for pipeline_run_dict in list(yaml.safe_load_all(f)):
                try:
                    pipeline_run.validate_pipeline_run(pipeline_run_dict)
                except Exception as e:
                    self.fail(e)

    def _validate_previous_pipeline_run_ids(self, pipeline_run_save_path):
        ids = set()
        prev_ids = set()
        with open(pipeline_run_save_path, 'r') as f:
            for pipeline_run_dict in list(yaml.safe_load_all(f)):
                ids.add(pipeline_run_dict['id'])
                if 'previous_pipeline_run' in pipeline_run_dict:
                    prev_ids.add(pipeline_run_dict['previous_pipeline_run']['id'])
        self.assertTrue(
            prev_ids.issubset(ids),
            'Some previous pipeline run ids {} are not in the set of pipeline run ids {}'.format(prev_ids, ids)
        )

    def test_fit_multi_input(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self._assert_standard_output_metadata()

    def test_fit_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-pipeline')
        output_csv_path = os.path.join(self.test_dir, 'output.csv')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--save',
            fitted_pipeline_path,
            '--expose-produced-outputs',
            self.test_dir,
            '--output',
            output_csv_path,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'fitted-pipeline',
            'output.csv',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json'
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=11486, outputs_path='outputs.0/data.csv')
        self._assert_prediction_sum(prediction_sum=11486, outputs_path='output.csv')

    def test_produce_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-no-problem-pipeline')
        output_csv_path = os.path.join(self.test_dir, 'output.csv')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--save',
            fitted_pipeline_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        arg = [
            '',
            'produce',
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--output',
            output_csv_path,
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'fitted-no-problem-pipeline',
            'output.csv',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json'
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=11486, outputs_path='outputs.0/data.csv')
        self._assert_prediction_sum(prediction_sum=11486, outputs_path='output.csv')

    def test_fit_produce_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        output_csv_path = os.path.join(self.test_dir, 'output.csv')
        arg = [
            '',
            'fit-produce',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--output',
            output_csv_path,
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'output.csv',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json'
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)
        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='outputs.0/data.csv')
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='output.csv')

    def test_nonstandard_fit_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-pipeline')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'semi-standard-pipeline.json'),
            '--save',
            fitted_pipeline_path,
            '--expose-produced-outputs',
            self.test_dir,
            '--not-standard-pipeline',
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'fitted-pipeline',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'outputs.1/data.csv',
            'outputs.1/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=10791, outputs_path='outputs.0/data.csv')
        self._assert_nonstandard_output(outputs_name='outputs.1')

    def test_nonstandard_produce_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-pipeline')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'semi-standard-pipeline.json'),
            '--save',
            fitted_pipeline_path,
            '--not-standard-pipeline'
        ]
        self._call_cli_runtime_without_fail(arg)

        arg = [
            '',
            'produce',
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'fitted-pipeline',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'outputs.1/data.csv',
            'outputs.1/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json'
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=10791, outputs_path='outputs.0/data.csv')
        self._assert_nonstandard_output(outputs_name='outputs.1')

    def test_nonstandard_fit_produce_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit-produce',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'semi-standard-pipeline.json'),
            '--expose-produced-outputs',
            self.test_dir,
            '--not-standard-pipeline',
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'outputs.1/data.csv',
            'outputs.1/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)
        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=11272, outputs_path='outputs.0/data.csv')
        self._assert_nonstandard_output(outputs_name='outputs.1')

    def test_fit_produce_multi_input(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit-produce',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertEqual(utils.list_files(self.test_dir), [
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json',
        ])

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)
        self._assert_standard_output_metadata()
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='outputs.0/data.csv')

    def test_fit_score(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit-score',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-forest-classifier.yml'),
            '--scores',
            os.path.join(self.test_dir, 'scores.csv'),
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(os.path.join(self.test_dir, 'scores.csv'))
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0]])

    def test_fit_score_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit-score',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-classifier.yml'),
            '--scoring-pipeline',
            os.path.join(PIPELINE_DIR, 'fake_compute_score.yml'),
            # this argument has no effect
            '--metric',
            'F1_MACRO',
            '--metric',
            'ACCURACY',
            '--scores',
            os.path.join(self.test_dir, 'scores.csv'),
            '-O',
            pipeline_run_save_path,
        ]
        logging_records = self._call_cli_runtime_without_fail(arg)

        self.assertEqual(len(logging_records), 1)
        self.assertEqual(logging_records[0].msg, "Not all provided hyper-parameters for the scoring pipeline %(pipeline_id)s were used: %(unused_params)s")

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(os.path.join(self.test_dir, 'scores.csv'))
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0]])

    def _fit_iris_random_forest(
        self, *, predictions_path=None, fitted_pipeline_path=None, pipeline_run_save_path=None
    ):
        if pipeline_run_save_path is None:
            pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-forest-classifier.yml'),
            '-O',
            pipeline_run_save_path
        ]
        if predictions_path is not None:
            arg.append('--output')
            arg.append(predictions_path)
        if fitted_pipeline_path is not None:
            arg.append('--save')
            arg.append(fitted_pipeline_path)

        self._call_cli_runtime_without_fail(arg)

    def _fit_iris_random_classifier_without_problem(self, *, fitted_pipeline_path):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-classifier.yml'),
            '-O',
            pipeline_run_save_path
        ]
        if fitted_pipeline_path is not None:
            arg.append('--save')
            arg.append(fitted_pipeline_path)

        self._call_cli_runtime_without_fail(arg)

    def test_fit(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-pipeline')
        self._fit_iris_random_forest(fitted_pipeline_path=fitted_pipeline_path)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self.assertTrue(os.path.isfile(fitted_pipeline_path))
        self.assertTrue(os.path.isfile(pipeline_run_save_path))

    def test_evaluate(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'evaluate',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-forest-classifier.yml'),
            '--data-pipeline',
            os.path.join(PIPELINE_DIR, 'data-preparation-no-split.yml'),
            '--scores',
            scores_path,
            '--metric',
            'ACCURACY',
            '--metric',
            'F1_MACRO',
            '-O',
            pipeline_run_save_path
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(scores_path)
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed', 'fold'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0, 0], ['F1_MACRO', 1.0, 1.0, 0, 0]])

    def test_evaluate_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'evaluate',
            '--input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-classifier.yml'),
            '--data-pipeline',
            os.path.join(PIPELINE_DIR, 'data-preparation-no-split.yml'),
            '--scoring-pipeline',
            os.path.join(PIPELINE_DIR, 'fake_compute_score.yml'),
            # this argument has no effect
            '--metric',
            'ACCURACY',
            '--scores',
            scores_path,
            '-O',
            pipeline_run_save_path
        ]
        logging_records = self._call_cli_runtime_without_fail(arg)

        self.assertEqual(len(logging_records), 1)
        self.assertEqual(logging_records[0].msg, "Not all provided hyper-parameters for the scoring pipeline %(pipeline_id)s were used: %(unused_params)s")

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(scores_path)
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed', 'fold'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0, 0]])

    def test_score(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'iris-pipeline')
        self._fit_iris_random_forest(fitted_pipeline_path=fitted_pipeline_path)
        self.assertTrue(os.path.isfile(fitted_pipeline_path))

        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'score',
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--scores',
            scores_path,
            '--metric',
            'F1_MACRO',
            '--metric',
            'ACCURACY',
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self.assertTrue(os.path.isfile(scores_path), 'scores were not generated')

        dataframe = pandas.read_csv(scores_path)

        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed'])
        self.assertEqual(dataframe.values.tolist(), [['F1_MACRO', 1.0, 1.0, 0], ['ACCURACY', 1.0, 1.0, 0]])

    def test_score_without_problem_without_metric(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'iris-pipeline')
        self._fit_iris_random_classifier_without_problem(fitted_pipeline_path=fitted_pipeline_path)
        self.assertTrue(os.path.isfile(fitted_pipeline_path))

        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'score',
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--scoring-pipeline',
            os.path.join(PIPELINE_DIR, 'fake_compute_score.yml'),
            '--scores',
            scores_path,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self.assertTrue(os.path.isfile(scores_path), 'scores were not generated')

        dataframe = pandas.read_csv(scores_path)

        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0]])

    def test_score_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'iris-pipeline')
        self._fit_iris_random_classifier_without_problem(fitted_pipeline_path=fitted_pipeline_path)
        self.assertTrue(os.path.isfile(fitted_pipeline_path))

        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'score',
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--scoring-pipeline',
            os.path.join(PIPELINE_DIR, 'fake_compute_score.yml'),
            # this argument has no effect
            '--metric',
            'ACCURACY',
            '--scores',
            scores_path,
            '-O',
            pipeline_run_save_path,
        ]
        logging_records = self._call_cli_runtime_without_fail(arg)

        self.assertEqual(len(logging_records), 1)
        self.assertEqual(logging_records[0].msg, "Not all provided hyper-parameters for the scoring pipeline %(pipeline_id)s were used: %(unused_params)s")

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

        self.assertTrue(os.path.isfile(scores_path), 'scores were not generated')

        dataframe = pandas.read_csv(scores_path)

        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0]])

    def test_produce(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'iris-pipeline')
        self._fit_iris_random_forest(fitted_pipeline_path=fitted_pipeline_path)
        self.assertTrue(os.path.isfile(fitted_pipeline_path))

        arg = [
            '',
            'produce',
            '--fitted-pipeline',
            fitted_pipeline_path,
            '--test-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)

    def test_score_predictions(self):
        predictions_path = os.path.join(self.test_dir, 'predictions.csv')
        self._fit_iris_random_forest(predictions_path=predictions_path)
        self.assertTrue(os.path.isfile(predictions_path))

        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'score-predictions',
            '--score-input',
            os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '--problem',
            os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '--predictions',
            predictions_path,
            '--metric',
            'ACCURACY',
            '--metric',
            'F1_MACRO',
            '--scores',
            scores_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self.assertTrue(os.path.isfile(scores_path), 'scores were not generated')

        dataframe = pandas.read_csv(scores_path)

        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0], ['F1_MACRO', 1.0, 1.0]])

    def test_sklearn_dataset_fit_produce(self):
        self._create_sklearn_iris_problem_doc()

        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        arg = [
            '',
            'fit-produce',
            '--input',
            'sklearn://iris',
            '--input',
            'sklearn://iris',
            '--problem',
            os.path.join(self.test_dir, 'problemDoc.json'),
            '--test-input',
            'sklearn://iris',
            '--test-input',
            'sklearn://iris',
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        self.assertEqual(utils.list_files(self.test_dir), [
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'problemDoc.json',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json'
        ])
        self._assert_standard_output_metadata(prediction_type='numpy.int64')
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='outputs.0/data.csv')

    def test_sklearn_dataset_fit_produce_without_problem(self):
        output_csv_path = os.path.join(self.test_dir, 'output.csv')
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        fitted_pipeline_path = os.path.join(self.test_dir, 'fitted-pipeline')
        arg = [
            '',
            'fit-produce',
            '--input',
            'sklearn://iris',
            '--test-input',
            'sklearn://iris',
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-classifier.yml'),
            '--save',
            fitted_pipeline_path,
            '--output',
            output_csv_path,
            '--expose-produced-outputs',
            self.test_dir,
            '-O',
            pipeline_run_save_path,
        ]

        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        self.assertEqual(utils.list_files(self.test_dir), [
            'fitted-pipeline',
            'output.csv',
            'outputs.0/data.csv',
            'outputs.0/metadata.json',
            'pipeline_run.yml',
            'steps.0.produce/data.csv',
            'steps.0.produce/metadata.json',
            'steps.1.produce/data.csv',
            'steps.1.produce/metadata.json',
            'steps.2.produce/data.csv',
            'steps.2.produce/metadata.json',
        ])
        self._assert_standard_output_metadata(prediction_type='numpy.int64')
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='outputs.0/data.csv')
        self._assert_prediction_sum(prediction_sum=10648, outputs_path='output.csv')

    def _create_sklearn_iris_problem_doc(self):
        with open(os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'), 'r', encoding='utf8') as problem_doc_file:
            problem_doc = json.load(problem_doc_file)

        problem_doc['inputs']['data'][0]['datasetID'] = 'sklearn://iris'

        with open(os.path.join(self.test_dir, 'problemDoc.json'), 'x', encoding='utf8') as problem_doc_file:
            json.dump(problem_doc, problem_doc_file)

    def test_sklearn_dataset_evaluate(self):
        self._create_sklearn_iris_problem_doc()

        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'evaluate',
            '--input',
            'sklearn://iris',
            '--problem',
            os.path.join(self.test_dir, 'problemDoc.json'),
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-forest-classifier.yml'),
            '--data-pipeline',
            os.path.join(PIPELINE_DIR, 'data-preparation-no-split.yml'),
            '--scores',
            scores_path,
            '--metric',
            'ACCURACY',
            '--metric',
            'F1_MACRO',
            '-O',
            pipeline_run_save_path
        ]
        self._call_cli_runtime_without_fail(arg)

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(scores_path)
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed', 'fold'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0, 0], ['F1_MACRO', 1.0, 1.0, 0, 0]])

    def test_sklearn_dataset_evaluate_without_problem(self):
        pipeline_run_save_path = os.path.join(self.test_dir, 'pipeline_run.yml')
        scores_path = os.path.join(self.test_dir, 'scores.csv')
        arg = [
            '',
            'evaluate',
            '--input',
            'sklearn://iris',
            '--pipeline',
            os.path.join(PIPELINE_DIR, 'random-classifier.yml'),
            '--data-pipeline',
            os.path.join(PIPELINE_DIR, 'data-preparation-no-split.yml'),
            '--scoring-pipeline',
            os.path.join(PIPELINE_DIR, 'fake_compute_score.yml'),
            # this argument has no effect
            '--metric',
            'ACCURACY',
            '--scores',
            scores_path,
            '-O',
            pipeline_run_save_path
        ]
        logging_records = self._call_cli_runtime_without_fail(arg)

        self.assertEqual(len(logging_records), 1)
        self.assertEqual(logging_records[0].msg, "Not all provided hyper-parameters for the scoring pipeline %(pipeline_id)s were used: %(unused_params)s")

        self._assert_valid_saved_pipeline_runs(pipeline_run_save_path)
        self._validate_previous_pipeline_run_ids(pipeline_run_save_path)

        dataframe = pandas.read_csv(scores_path)
        self.assertEqual(list(dataframe.columns), ['metric', 'value', 'normalized', 'randomSeed', 'fold'])
        self.assertEqual(dataframe.values.tolist(), [['ACCURACY', 1.0, 1.0, 0, 0]])

    def _assert_prediction_sum(self, prediction_sum, outputs_path):
        if prediction_sum is not None:
            with open(os.path.join(self.test_dir, outputs_path), 'r') as csv_file:
                self.assertEqual(sum([int(v) for v in list(csv_file)[1:]]), prediction_sum)

    def _assert_standard_output_metadata(self, outputs_name='outputs.0', prediction_type='str'):
        with open(os.path.join(self.test_dir, outputs_name, 'metadata.json'), 'r') as metadata_file:
            metadata = json.load(metadata_file)

        self.assertEqual(
            metadata,
            [
                {
                    "selector": [],
                    "metadata": {
                        "dimension": {
                            "length": 150,
                            "name": "rows",
                            "semantic_types": ["https://metadata.datadrivendiscovery.org/types/TabularRow"],
                        },
                        "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
                        "semantic_types": ["https://metadata.datadrivendiscovery.org/types/Table"],
                        "structural_type": "d3m.container.pandas.DataFrame",
                    },
                },
                {
                    "selector": ["__ALL_ELEMENTS__"],
                    "metadata": {
                        "dimension": {
                            "length": 1,
                            "name": "columns",
                            "semantic_types": ["https://metadata.datadrivendiscovery.org/types/TabularColumn"],
                        }
                    },
                },
                {"selector": ["__ALL_ELEMENTS__", 0],
                 "metadata": {"name": "predictions", "structural_type": prediction_type}},
            ],
        )

    def _assert_nonstandard_output(self, outputs_name='outputs.1'):
        with open(os.path.join(self.test_dir, outputs_name, 'data.csv'), 'r') as csv_file:
            output_dataframe = pandas.read_csv(csv_file, index_col=False)
            learning_dataframe = pandas.read_csv(
                os.path.join(DATASET_DIR, 'iris_dataset_1/tables/learningData.csv'), index_col=False)
            self.assertTrue(learning_dataframe.equals(output_dataframe))

        with open(os.path.join(self.test_dir, outputs_name, 'metadata.json'), 'r') as metadata_file:
            metadata = json.load(metadata_file)

        self.assertEqual(
            metadata,
            [
                {
                    "metadata": {
                        "dimension": {
                            "length": 150,
                            "name": "rows",
                            "semantic_types": [
                                "https://metadata.datadrivendiscovery.org/types/TabularRow"
                            ]
                        },
                        "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
                        "semantic_types": [
                            "https://metadata.datadrivendiscovery.org/types/Table"
                        ],
                        "structural_type": "d3m.container.pandas.DataFrame"
                    },
                    "selector": []
                },
                {
                    "metadata": {
                        "dimension": {
                            "length": 6,
                            "name": "columns",
                            "semantic_types": [
                                "https://metadata.datadrivendiscovery.org/types/TabularColumn"
                            ]
                        }
                    },
                    "selector": [
                        "__ALL_ELEMENTS__"
                    ]
                },
                {
                    "metadata": {
                        "name": "d3mIndex",
                        "semantic_types": [
                            "http://schema.org/Integer",
                            "https://metadata.datadrivendiscovery.org/types/PrimaryKey"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        0
                    ]
                },
                {
                    "metadata": {
                        "name": "sepalLength",
                        "semantic_types": [
                            "http://schema.org/Float",
                            "https://metadata.datadrivendiscovery.org/types/Attribute"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        1
                    ]
                },
                {
                    "metadata": {
                        "name": "sepalWidth",
                        "semantic_types": [
                            "http://schema.org/Float",
                            "https://metadata.datadrivendiscovery.org/types/Attribute"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        2
                    ]
                },
                {
                    "metadata": {
                        "name": "petalLength",
                        "semantic_types": [
                            "http://schema.org/Float",
                            "https://metadata.datadrivendiscovery.org/types/Attribute"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        3
                    ]
                },
                {
                    "metadata": {
                        "name": "petalWidth",
                        "semantic_types": [
                            "http://schema.org/Float",
                            "https://metadata.datadrivendiscovery.org/types/Attribute"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        4
                    ]
                },
                {
                    "metadata": {
                        "name": "species",
                        "semantic_types": [
                            "https://metadata.datadrivendiscovery.org/types/CategoricalData",
                            "https://metadata.datadrivendiscovery.org/types/SuggestedTarget",
                            "https://metadata.datadrivendiscovery.org/types/Attribute"
                        ],
                        "structural_type": "str"
                    },
                    "selector": [
                        "__ALL_ELEMENTS__",
                        5
                    ]
                }
            ]
        )

    def test_pipeline_run_json_equals(self):
        pipeline_run_save_path1 = os.path.join(self.test_dir, 'pipeline_run1.yml')
        self._fit_iris_random_forest(pipeline_run_save_path=pipeline_run_save_path1)

        with open(pipeline_run_save_path1, 'r') as f:
            try:
                pipeline_runs1 = list(yaml.safe_load_all(f))
                for pipeline_run_dict in pipeline_runs1:
                    pipeline_run.validate_pipeline_run(pipeline_run_dict)
            except Exception as e:
                self.fail(e)

        pipeline_run_save_path2 = os.path.join(self.test_dir, 'pipeline_run2.yml')
        self._fit_iris_random_forest(pipeline_run_save_path=pipeline_run_save_path2)

        with open(pipeline_run_save_path2, 'r') as f:
            try:
                pipeline_runs2 = list(yaml.safe_load_all(f))
                for pipeline_run_dict in pipeline_runs2:
                    pipeline_run.validate_pipeline_run(pipeline_run_dict)
            except Exception as e:
                self.fail(e)

        for pipeline_run1, pipeline_run2 in zip(pipeline_runs1, pipeline_runs2):
            self.assertTrue(pipeline_run.PipelineRun.json_structure_equals(pipeline_run1, pipeline_run2))


if __name__ == '__main__':
    unittest.main()
